const Course = require('../models/course.js');
const mongoose = require('mongoose');
const auth = require('../auth.js');

// S39 Acitivity
// Create a single course
module.exports.addCourse = (data) => {
	console.log(data.isAdmin);

	if(data.isAdmin) {
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		});
		return newCourse.save().then((newCourse, error) => {
			if(error){
				return error;
			}
			return newCourse;
		}); 
	};
	// If the user is not admin, then return this message is a promise to avoid errors
	let message = Promise.resolve('User must be ADMIN to access this.');
	return message.then(value => {
		return {value};
	});
};

module.exports.getAllCourse = () => {
	return Course.find({/* all documents*/}).then(result => { 
		return result;
	}); 
};

// GET all Active courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive: true}).then(result => { 
		return result;
	}); 
};
/*
	1 function 1 return
	1 function 1 return(function 1 return)
*/

// GET specific course
module.exports.getCourse = (courseId) => {
	return Course.findById(courseId).then(result => {
		return result;
	});
};

module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true) {
		return Course.findByIdAndUpdate(courseId,
			{	// req.body
				name: newData.course.name,
				description: newData.course.description,
				price: newData.course.price
			}
		).then((updatedCourse, error) => {
			if(error) {
				return false
			}
			return true
		});
	}
	else {
		let message = Promise.resolve('User must be ADMIN to access this.');
		return message.then(value => {return value});
	};
};

// S40 Activity
// Archive course without auth
/* 
module.exports.archiveCourse = (courseId) => {
	return Course.findByIdAndUpdate(courseId, {
			isActive: false
	})
	.then((archiveCourse, error) => {
			if(error) {
				return false
			}
			return {
				message: "Course archived successfully!"
			};
		});
};
*/

// Archive course with auth
module.exports.archiveCourse = (courseId, newStatus) => {
	if(newStatus.isAdmin) {
		return Course.findByIdAndUpdate(courseId, {
			isActive: false // newStatus.course.isActive
		})
		.then((archiveCourse, error) => {
			if(error) {
				return false
			}
			return {
				message: "Course archived successfully!"
			}
		});
	}
	else {
		let message = Promise.resolve('User must be ADMIN to access this.');
		return message.then(value => {return value});
	};
};

