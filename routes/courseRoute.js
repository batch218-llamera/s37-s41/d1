const express = require('express'); // access package of express
const router = express.Router(); // use dot notation to access content of a package

const courseController = require('../controllers/courseController.js');
const auth = require('../auth.js');

// s39 Activity
// Create a single course with auth
router.post('/create', auth.verify, (req, res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin // auth.decode - is a method to retrieve user info from the bearer token // isAdmin - inside the payload
	}
	courseController.addCourse(data).then(resultFromController => res.send(resultFromController)); // then - returns a promise
});

/*
	router.post('/create', auth.verify, (req, res) => {
		const newCourse = {
			course: req.body,
			isAdmin: auth.decode(req.headers.authorization).isAdmin
		}
		courseController.addCourse(req.body)
			.then(resultFromController => res.send(resultFromController));
	});
*/

router.get('/all', (req, res) => {
	courseController.getAllCourse().then(resultFromController => res.send(resultFromController));
});

router.get('/active', (req, res) => {
	courseController.getActiveCourses().then(resultFromController =>
		res.send(resultFromController));
});

router.get('/:courseId', (req, res) => {
	courseController.getCourse(req.params.courseId).then(resultFromController => res.send(resultFromController));
});

// Updating a single course
// lclhst4000/crs001/update - valid
// lclhst4000/update/crs001 - error
router.patch('/:courseId/update', auth.verify, (req,res) => {
	const newData = {
		course: req.body, 	//req.headers.authorization contains jwt
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.updateCourse(req.params.courseId, newData).then(resultFromController => res.send(resultFromController));
});

// S40 Activity
// Archiving a single course
/*
router.patch('/:courseId/archive', auth.verify, (req, res) => {
	courseController.archiveCourse(req.params.courseId).then(resultFromController => { // params - for dynamic routing to accept courseId
		res.send(resultFromController);
	});
});
*/

router.patch('/:courseId/archive', auth.verify, (req, res) => {
	const newStatus = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.archiveCourse(req.params.courseId, newStatus).then(resultFromController => res.send(resultFromController));
});


module.exports = router;