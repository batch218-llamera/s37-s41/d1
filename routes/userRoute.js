
// dependencies
const express = require('express');

const router = express.Router(); // part of express package
const User = require('../models/user.js');
const userController = require('../controllers/userController.js')
const auth = require('../auth.js');

router.post('/checkEmail', (req, res) => {
	// we can only use 'req.body' in post
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController));
});

router.post('/register', auth.verify, (req, res) => {
	const newUser = {
		user: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post('/details/:id', (req, res) => {
	userController.getProfile(req.params.id).then(resultFromController => res.send(resultFromController));
});

// S41 Activity
// Enroll a user
/*
router.post('/enroll', auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});
*/

router.post('/enroll', auth.verify, (req, res) => {
	const newEnrollee = {
		courseId: req.body.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.enroll(req.body).then(resultFromController => res.send(resultFromController));
});


module.exports = router;
