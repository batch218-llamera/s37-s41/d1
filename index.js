/*
	Terminal:
	npm init -y
	npm i express
	npm i mongoose
	npm i cors
	npm i bcrypt
	npm i jsonwebtoken
*/

// dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express(); // to create a express server/application

// course.js(model) >> userController > userRoutes
// routers
const userRoute = require('./routes/userRoute.js');
const courseRoute = require('./routes/courseRoute.js');

// middlewares - allows to bridge backend application (server) to our front end
app.use(cors()); // to allow cross origin resource sharing
app.use(express.json()); // to read json objects
app.use(express.urlencoded({extended: true})); // to read submitted data via forms

// Initializing routes
app.use('/users', userRoute);
app.use('/courses', courseRoute);

// connect to our MongoDB database
mongoose.connect('mongodb+srv://admin:admin@batch218-coursebooking.dafcna9.mongodb.net/courseBooking?retryWrites=true&w=majority',
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// prompts a message once connected
mongoose.connection.once('open', () => console.log('Now connected to Llamera-Mongo DB Atlas.'))

// prompts a message once connected to port 4000
app.listen(process.env.PORT || 3000, () =>
	{console.log(`API is now online on port ${process.env.PORT || 3000}`)});
// 3000, 4000, 5000, 8000 - Port numbers for web applications

